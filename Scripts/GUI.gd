extends Label

var tutorialNum = 0
#var root = get_parent().get_parent().get_parent()

func _process(delta):
	if self.name == "#ofDeaths":
		self.text = str(Global.deathCount)

	elif self.name == "Level":
		#if Global.level == Global.levelNumReset:
		#	self.text = "Test Area"
		#if root.name.substr(0,8) == "Tutorial":
		if Global.level < 1:
			self.text = "Level: Tutorial " + str(Global.tutorialNum)
		else:
			self.text = "Level " + str(Global.level)
		#self.text = str(get_parent().get_parent().get_parent().name)
