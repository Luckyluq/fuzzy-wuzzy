extends AudioStreamPlayer

func _process(delta):
	if !self.playing:
		self.play()
