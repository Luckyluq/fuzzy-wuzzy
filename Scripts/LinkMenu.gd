extends LinkButton

export(String) var scene_to_load

func _ready():
	self.connect("pressed", self, "on_pressed")

func on_pressed():
	if self.name == "Yes":
		get_tree().change_scene("res://Scenes/Levels/Tutorial_1.tscn")
	elif self.name == "No":
		Global.level = 1
		get_tree().change_scene("res://Scenes/Levels/Level_1.tscn")
	elif self.name == "Main Menu":
		Global.resetFull()
		get_tree().change_scene("res://Scenes/Main_Menu.tscn")
	elif self.name == "Exit":
		get_tree().quit()
		
	elif self.name == "Link":
		OS.shell_open("https://www.sealfurai.itch.io/fuzzywuzzy")
		
