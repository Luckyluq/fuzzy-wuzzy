extends Area2D

#var colTile = load("res://scripts/Colored_Tile.gd").new()
#var colTileParent = self.get_parent().get_parent().get_parent().get_node("World_Tile").get_node("Colored_Tile")
var state = ""
var index = 0 #0 sebagai placeholder

func _ready():
	self.connect("body_entered", self, "enterAltar")
	self.connect("body_exited", self, "exitAltar")
	
	#cuma untuk awal2, kalo nnt diganti isinya jadi ganti state nya
	if self.name.substr(6,1) == "N": #ngecek apakah substring index 6 == B, karena node name bisa berupa Altar_None2 dst
		state = "None"
		index = 0
	if self.name.substr(6,1) == "B":
		state = "Blue"
		index = 1
	if self.name.substr(6,1) == "G":
		state = "Green"
		index = 2
	if self.name.substr(6,1) == "P":
		state = "Purple"
		index = 3
	if self.name.substr(6,1) == "R":
		state = "Red"
		index = 4
		
	setColTileState(index, state)
	print("set global colTileState di ready altar called, index " + str(index) + ", state " + state) #testing
	
	#switchColor(state) #nyesuain warna utk di awal
	#print("altar is " + Type)
	#$AltarSprite.play(Type)

func _process(delta):
	if $AltarSprite.animation != state:
		$AltarSprite.play(state)
	#switchColor(state) #biar nyesuain color tile dengan altar yang menyala

#func switchColor(state):
#	for _i in colTileParent.get_children():
#		colTile.switchColor(state)

func enterAltar(body):
	if body.get_name() == "Fuzzy":
		Global.fuzOnTop = true
		Global.altarOnBot = self #nandain yg disentuh itu altar mana
		print("passed altar " + state) #testing
	#if dimasukkin, ganti state

func exitAltar(body):
	if body.get_name() == "Fuzzy":
		Global.fuzOnTop = false
		Global.altarOnBot = null #ilangin isinya biar ngurangin bug
		print("exited altar " + state) #testing

func setColTileState(index, state): #ganti state sesuai warna, urutan: NONE, BLUE, GREEN, PURPLE, RED
	Global.colTileState[index] = state

#func insertWuzzy(color):
#	pass
	
func removeWuzzy():
	#lengkapin
	pass

#Tutorial 2, 3 & 4
func _on_Altar_None_body_entered(body):
	var parent = get_parent().get_parent().get_parent()
	#Tutorial 2
	if body.get_name() == "Fuzzy" and parent.name == "Tutorial_2":
		parent.onAltar()
	#Tutorial 4
	if body.get_name() == "Fuzzy" and parent.name == "Tutorial_4":
		parent.onAltar()
