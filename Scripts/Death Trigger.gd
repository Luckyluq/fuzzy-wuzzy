extends Area2D

func _on_Area2D_body_entered(body):
	if body.get_name() == "Fuzzy":
		#Menentukan respawn di mana jika jatuh
		if Global.lastRespawn == "start":
			body.position = get_parent().get_parent().get_node("Poles").get_node("Pole_Start").position
		elif Global.lastRespawn == "check":
			body.position = get_parent().get_parent().get_node("Poles").get_node("Pole_Check").position
			
		Global.deathCount += 1 #Jangan lupa kalo balik ke Main Menu, reset deathCount

