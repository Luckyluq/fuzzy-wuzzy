extends Node

#var startingWuzzies = [["r", "", ""],[],[]]
#var curWuz = getCurrentWuzzies()

var BlueInv = preload("res://Assets/Spritesheet/Inventory/Blue Inventory.png")
var GreenInv = preload("res://Assets/Spritesheet/Inventory/Green Inventory.png")
var PurpleInv = preload("res://Assets/Spritesheet/Inventory/Purple Inventory.png")
var RedInv = preload("res://Assets/Spritesheet/Inventory/Red Inventory.png")
var chosenColor

onready var left = get_node("Left")
onready var mid = get_node("Mid")
onready var right = get_node("Right")
onready var invenArray = [left, mid, right] #array isinya sprite kiri, tengah, kanan

func _ready():
	print("inven " + str(getCurrentWuzzies()))

func _process(delta):
	#ganti GUI inventory secara realtime
	for i in range(3):
		if getCurrentWuzzies()[i] == "Blue":
			chosenColor = BlueInv
		elif getCurrentWuzzies()[i] == "Green":
			chosenColor = GreenInv
		elif getCurrentWuzzies()[i] == "Purple":
			chosenColor = PurpleInv
		elif getCurrentWuzzies()[i] == "Red":
			chosenColor = RedInv
		elif getCurrentWuzzies()[i] == "":
			chosenColor = null #kalo isinya "" berarti ilangin texture
		invenArray[i].set_texture(chosenColor) #nyesuain urutan array dengan warnanya
		
	#Global.currentWuzzies = curWuz #balikin lagi biar value masing2 sama, ngindarin bug bisa dobel

#func getStartingWuzzies(index):
#	return startingWuzzies[index]

func getCurrentWuzzies():
	return Global.currentWuzzies
	
func swap(direction): #swap wuzzy
	var temp1
	var temp2
	if getCurrentWuzzies()[2] != "": #cek inven kanan, artinya ada 3 di inven
		if direction == "left": #swap dengan kiri
			temp1 = getCurrentWuzzies()[1]; getCurrentWuzzies()[1] = getCurrentWuzzies()[0] #kiri geser ke tengah
			temp2 = getCurrentWuzzies()[2]; getCurrentWuzzies()[2] = temp1 #tengah geser ke kanan
			getCurrentWuzzies()[0] = temp2 #kanan geser ke kiri
			print("swapped left with 3 wuzzies") #testing
			print(getCurrentWuzzies()) #testing
		else: #swap dengan kanan
			temp1 = getCurrentWuzzies()[1]; getCurrentWuzzies()[1] = getCurrentWuzzies()[2] #kanan geser ke tengah
			temp2 = getCurrentWuzzies()[0]; getCurrentWuzzies()[0] = temp1 #tengah geser ke kiri
			getCurrentWuzzies()[2] = temp2 #kiri geser ke kanan
			print("swapped right with 3 wuzzies") #testing
			print(getCurrentWuzzies()) #testing
	elif getCurrentWuzzies()[0] != "": #cek inven kiri, artinya cuma ada 2 di inven
		temp1 = getCurrentWuzzies()[1]; getCurrentWuzzies()[1] = getCurrentWuzzies()[0] #kiri geser ke tengah
		getCurrentWuzzies()[0] = temp1 #tengah geser ke kanan
		print("swapped with 2 wuzzies") #testing
		print(getCurrentWuzzies()) #testing
	else: #artinya cuma ada 1 atau gaada di inven, nothing happens
		print("no other wuzzies to swap") #testing
		print(getCurrentWuzzies()) #testing
	
func checkInvenEmpty():	
	if getCurrentWuzzies()[1] == "":
		return true
		
func checkInvenDupe(color):
	var count = getCurrentWuzzies().count(color)
	if count > 0:
		return true
	return false

#Cek kalo ada slot kosong, masukkin. Urutan: tengah, kiri, kanan
func insertWuzzy(color):
	var printVal = "inserted " + color + " to inventory" #testing
	if !checkInvenDupe(color): #check apakah ada dupe, kalo iya maka ga dimasukkin
		if checkInvenEmpty():
			getCurrentWuzzies()[1] = color
		elif getCurrentWuzzies()[0] == "":
			getCurrentWuzzies()[0] = color
		elif getCurrentWuzzies()[2] == "":
			getCurrentWuzzies()[2] = color
		else:
			printVal = "inserting " + color + " but inventory is full" #testing
		print(printVal) #testing
	else:
		print(color + " already in inventory")

func useWuzzy():
	if Global.fuzOnTop: #cek apakah di altar
		if Global.altarOnBot.state == "None": #cek apakah altarnya NONE (mau masukkin ke altar)
			if !checkInvenEmpty(): #kalo ada isi inventory
				#biar 1 warna wuzzy cuma bisa di 1 altar
				if Global.colTileState[getColTileStateIndex(getCurrentWuzzies()[1])] != getCurrentWuzzies()[1]: #biar 1 warna wuzzy cuma bisa di 1 altar
					print(getCurrentWuzzies()[1] + " wuzzy used") #testing
					Global.altarOnBot.state = getCurrentWuzzies()[1] #assign warna di inven tengah ke altar None
					setColTileState(getCurrentWuzzies()[1], getCurrentWuzzies()[1]) #ganti warna color tile dari xT ke xF
					#insert sfx
					print ("altar None becomes altar " + Global.altarOnBot.state) #testing
					#update inventory
					getCurrentWuzzies()[1] = getCurrentWuzzies()[0] #kiri geser ke tengah
					getCurrentWuzzies()[0] = getCurrentWuzzies()[2] #kanan geser ke kiri
					getCurrentWuzzies()[2] = ""
					print(getCurrentWuzzies()) #testing
					#TAMBAHIN ANIMASI
				else:
					print("udah ada warna " + str(getColTileStateIndex(getCurrentWuzzies()[1])) + " di yg nyala") #testing
			else:
				print("cannot use, no fuzzy in inventory")
		else:
			print("altar is not None, but " + Global.altarOnBot.state) #testing
	else:
		print("not on altar") #testing
			
func removeWuzzy():
	var temp 
	if Global.fuzOnTop: #cek apakah di altar
		if Global.altarOnBot.state != "None": #cek apakah altarnya ada isinya (selain None)
			temp = Global.altarOnBot.state
			Global.altarOnBot.state = "None" #kosongin
			setColTileState(temp, "Off") #ganti warna color tile dari xF ke xT
			print ("altar " + temp + " removed") #testing
			#insert sfx
	
func discardWuzzy():
	var discarded = ""
	if !checkInvenEmpty(): #kalo ada isi inventory
		discarded = getCurrentWuzzies()[1]
		getCurrentWuzzies()[1] = getCurrentWuzzies()[0] #kiri geser ke tengah
		getCurrentWuzzies()[0] = getCurrentWuzzies()[2] #kanan geser ke kiri
		getCurrentWuzzies()[2] = ""
		#insert sfx
		print(discarded + " wuzzy discarded") #testing
		print(getCurrentWuzzies()) #testing
	else:
		print("cannot discard, no fuzzy in inventory")
		
func setColTileState(state, nextState): #ganti state sesuai warna, urutan: NONE, BLUE, GREEN, PURPLE, RED
	var index
	if state == "None":
		index = 0
	elif state == "Blue":
		index = 1
	elif state == "Green":
		index = 2
	elif state == "Purple":
		index = 3
	elif state == "Red":
		index = 4
	Global.colTileState[index] = nextState

func getColTileStateIndex(state): #cek index buat di colTileState
	var index
	if state == "None":
		index = 0
	elif state == "Blue":
		index = 1
	elif state == "Green":
		index = 2
	elif state == "Purple":
		index = 3
	elif state == "Red":
		index = 4
	return index
