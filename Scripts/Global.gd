extends Node

var checkPassed = false
var deathCount = 0
var lastRespawn = "start"
var ressingStart = false
var ressingCheck = false
var outOfTutorial = false

var fuzOnTop = false
var altarOnBot = null

var colTileState = ["None", "Off", "Off", "Off", "Off"] #NONE, BLUE, GREEN, PURPLE, RED

var levelNumReset = -4  #ganti dgn menyesuaikan jumlah tutorial. tutorial ada di -x s/d 0
var level = levelNumReset #level -x sampai 0 adalah tutorial, value meningkat di Portal biar value = 1 di level 1
var tutorialNum = 1
var currentWuzzies = ["","",""]

#var dialogLevel = 0
var dialogIndex = 0

func resetPartial(): #dipake kalo pindah level, ga semua direset
	checkPassed = false
	lastRespawn = "start"
	fuzOnTop = false
	altarOnBot = null
	currentWuzzies = ["","",""]
	colTileState = ["None", "Off", "Off", "Off", "Off"]
	dialogIndex = 0
	
func resetFull():
	resetPartial()
	deathCount = 0
	level = levelNumReset
	tutorialNum = 1
	outOfTutorial = false
	#dialogLevel = 0

func outOfTutorial():
	outOfTutorial = true
	deathCount = 0 #reset biar death di tutorial ga ngikut di main game
