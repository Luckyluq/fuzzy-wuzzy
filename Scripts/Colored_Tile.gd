extends StaticBody2D

var anim = ""
var disableColl
var modulateA = 255
var index

func _ready():	
	#cuma untuk awal2, kalo nnt diganti isinya jadi ganti anim nya
	#ngecek apakah substring index 6 == B, karena node name bisa berupa BT2 dst 
	nameToIndex(self) #dapet index & disableColl pas instancing
	#$AltarSprite.play(Type)

func _process(delta):
	#print(anim)
	#print($AnimatedSprite.animation)
	#$AnimatedSprite.play(anim)
	#if $AnimatedSprite.animation != anim:
	#	$AnimatedSprite.play(anim)
	
	setColor(index)
	$CollisionShape2D.set_deferred("disabled", disableColl)
	modulate.a8 = modulateA
	
	if $AnimatedSprite.animation != anim:
		$AnimatedSprite.play(anim)
	anim = "" #reset animnya biar bisa work & queue nya ga kebanyakan
	modulateA = 255# reset modulate
		
func nameToIndex(instance): #dapetin index buat nentuin warna & animasinya apa pas awal instancing
	if instance.name.substr(8,1) == "B": #Blue
		index = 1
	elif instance.name.substr(8,1) == "G": #Green
		index = 2
	elif instance.name.substr(8,1) == "P": #Purple
		index = 3
	elif instance.name.substr(8,1) == "R": #Red
		index = 4
		
	#if instance.name.substr(9,1) == "T": #Transparent
	#	disableColl = true
	#	Global.colTileState[index] = "Off" #menandakan kalo ada tipe warna tersebut (sesuai index), tapi transparent
	#elif instance.name.substr(9,1) == "F": #Full
	#	disableColl = false

func setColor(index): #ganti state sesuai warna, dicek secara runtime
	if index == 1:
		anim += "B"
	elif index == 2:
		anim += "G"
	elif index == 3:
		anim += "P"
	elif index == 4:
		anim += "R"

	if Global.colTileState[index] == "Off":
		disableColl = true
		anim += "T"
		modulateA = 100
	elif Global.colTileState[index] == "Blue":
		disableColl = false
		anim += "F"
	elif Global.colTileState[index] == "Green":
		disableColl = false
		anim += "F"
	elif Global.colTileState[index] == "Purple":
		disableColl = false
		anim += "F"
	elif Global.colTileState[index] == "Red":
		disableColl = false
		anim += "F"
