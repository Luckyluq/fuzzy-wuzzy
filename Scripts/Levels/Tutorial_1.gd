extends Node2D

onready var dialog = get_node("Objects/Dialogue/Extra_Big_Dialogue")

var dialogList = [
	"Hey [color=#ffc826]Fuzzy[/color]! Going on an adventure? Hop inside the [color=#f31edf]PORTAL[/color] " \
	+ "to go proceed! \nPress [color=#ffc826]LEFT ARROW[/color] and [color=#ffc826]RIGHT ARROW[/color] to move, " \
	+ "and press [color=#ffc826]UP ARROW[/color] or [color=#ffc826]SPACE[/color] to jump."
	]

func _ready():
	dialog.loadDialog(dialogList)
