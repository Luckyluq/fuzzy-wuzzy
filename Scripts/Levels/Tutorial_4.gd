extends Node2D

onready var dialog = get_node("Objects/Dialogue/Extra_Big_Dialogue")
var fullInven = false
var fullInvenText = false
var discarded = false
var discardForRed = false
var discardForRedText = false
var onAltar = false
var onAltarText = false

var dialogList = [
	"There are 4 types of Wuzzies: [color=#001cff]BLUE[/color], [color=#00ff15]GREEN[/color], " \
	+ "[color=#cd00ff]PURPLE[/color], and [color=#ff0000]RED[/color]. Try picking them up.",
	"You can hold up to [color=#ffc826]3 WUZZIES[/color] of [color=#ffc826]DIFFERENT COLORS[/color] in your " \
	+ "inventory. If you want to discard a Wuzzy to pick up another color, press [color=#ffc826]R[/color].",
	"Now you have a free slot in your inventory! Grab the [color=#ff0000]RED WUZZY[/color] and " \
	+ "insert it into the [color=grey]ALTAR[/color] to proceed.",
	"To select which Wuzzy you wish to insert, swap the Wuzzies in your inventory with " \
	+ "[color=#ffc826]Q[/color] or [color=#ffc826]E[/color]. Remember to press " \
	+ "[color=#ffc826]S[/color] to insert a Wuzzy.",
	"Nicely done. Before you press onwards, do note that a [color=#ffc826]FUZZY[/color] of " \
	+ "[color=#ffc826]EACH COLOR[/color] can only be activated at [color=#ffc826]ONE ALTAR[/color] at a time. \nNow, Onwards!"
	]

func _ready():
	dialog.loadDialog(dialogList)
	
func _process(delta):
	if Global.currentWuzzies[2] != "" and !fullInven:
		fullInven()
		
	if fullInven and !fullInvenText: #apakah inven full
		dialog.loadDialog(dialogList)
		fullInvenText = true
		
	if fullInvenText and !discardForRed and !discardForRedText and !onAltarText and Input.is_action_just_pressed("discard_wuzzy"): #apakah sudah dijalanin teks full inven
		dialog.loadDialog(dialogList)
		discardForRed = true
		
	if onAltar and discardForRed and !onAltarText: #kalo udah ke altar dan punya red wuzzy
		if Global.currentWuzzies[0] == "Red" or Global.currentWuzzies[1] == "Red" or Global.currentWuzzies[2] == "Red": #kalo punya merah di inven
			dialog.loadDialog(dialogList)
			onAltarText = true
		
	if onAltarText and discardForRed and Input.is_action_just_pressed("use_wuzzy"):
		if Global.colTileState[4] == "Red": #kalo yg dimasukkin adalah red
			dialog.loadDialog(dialogList)

#Tutorial 4
func fullInven():
	fullInven = true
	print("full inven") #testing
	
func onAltar():
	onAltar = true
	print("onAltar called") #testing
