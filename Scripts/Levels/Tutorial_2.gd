extends Node2D

onready var dialog = get_node("Objects/Dialogue/Extra_Big_Dialogue")
var gotWuzzy = false
var gotWuzzyText = false
var onAltar = false
var onAltarText = false

var dialogList = [
	"You see that [color=#93bffd]INVISIBLE BLUE BLOCK?[/color] You'll walk right through it and " \
	+ "fall, so don't go to the portal yet! Pickup that [color=#001cff]BLUE BALL[/color] first!",
	"What you've just picked up is a [color=#001cff]BLUE WUZZY[/color], and it's stored in your " \
	+ "inventory. You can insert a Wuzzy into the [color=grey]ALTAR[/color] in front of you.",
	"Press [color=#ffc826]S[/color] when on an altar to insert the Wuzzy from the middle slot of " \
	+ "your inventory into the altar. It is marked with the [color=gray]WHITE RECTANGLE[/color].",
	"Great! the invisible blue blocks have become [color=#001cff]FULL BLUE BLOCKS[/color] and are " \
	+ "safe to cross now. Onwards!"
	]

func _ready():
	dialog.loadDialog(dialogList)
	
func _process(delta):
	if gotWuzzy and !gotWuzzyText: #apakah sudah dapet wuzzy dan teksnya udh muncul
		dialog.loadDialog(dialogList)
		gotWuzzyText = true
	
	if gotWuzzy and onAltar and !onAltarText: #apakah sudah dapet wuzzy, ada di altar dan teksnya udh muncul
		dialog.loadDialog(dialogList)
		onAltarText = true
		
	if onAltarText and Input.is_action_just_pressed("use_wuzzy") and Global.colTileState[1] == "Blue":
		#apakah sudah masuk blue nya
		dialog.loadDialog(dialogList)

func gotWuzzy():
	gotWuzzy = true
	print("gotWuzzy") #testing

func onAltar():
	onAltar = true
	print("onAltar") #testing

#Other triggers see Altar.gd & Wuzzy.gd
