extends Node2D

onready var dialog = get_node("Objects/Dialogue/Extra_Big_Dialogue")
var gotDown = false
var gotDownText = false
var wantUp = false
var wantUpText = false
var gotUp = false
var gotUpText = false
var reachCheck = false
var reachCheckText = false
var gotDownCheck = false
var gotDownCheckText = false
var respawnedCheckText = false
var almostPass = false
var almostPassText = false

var dialogList = [
	"Now you can see there are 2 [color=grey]POLES[/color] here. The one with the [color=#ff0000]GO[/color]" \
	+ " sign is your [color=#ff0000]STARTING POINT[/color], where you will [color=#ffc826]RESPAWN[/color] if " \
	+ "you fall off these platforms and die. For now, let's get those Wuzzies down there!",
	"You've got the Wuzzies, but now you're stuck :/ \nDon't worry, you can get back up there! Remember " \
	+ "that [color=#ff0000]GO POLE[/color]?",
	"Besides falling off the platforms, you can also pop yourself up to [color=#ffc826]RESPAWN[/color] " \
	+ "at the [color=#ff0000]STARTING POINT[/color] by pressing [color=#ffc826]Z[/color].",
	"Great! Don't worry, Fuzzies are [color=#ffc826]IMMORTAL[/color]. Popping up yourself will only Increase " \
	+ "your [color=#ff0000]DEATHCOUNT[/color]. Other than that, it can even be [color=#ffc826]USEFUL[/color]" \
	+ ", just like the situation you just got out of!",
	"The pole you've just passed is a [color=#ff0000]CHECKPOINT[/color]. Noticed now there's a " \
	+ "[color=#ff0000]FLAG[/color]attached to it? That means you've activated this level's " \
	+ "checkpoint.\nLet's activate the altar down there with a blue wuzzy.",
	"Just like respawning at the starting point, you can also [color=#ffc826]RESPAWN[/color] at the " \
	+ "[color=#ff0000]CHECKPOINT[/color] by pressing [color=#ffc826]C[/color].",
	"You can only respawn at the checkpoint once you've activated it, " \
	+ "but you'll be able to respawn at the starting point any time. " \
	+"You will also respawn at the latest pole you passed if you fall off the platforms.",
	"Welp little Fuzzy, that's all I can teach you. The rest is up to you.\nAfter all, it is your " \
	+ "adventure! I'll reset your deathcount on the first level, good luck on your journey!"
	]

func _ready():
	dialog.loadDialog(dialogList)
	
func _process(delta):
	if gotDown and !gotDownText:
		dialog.loadDialog(dialogList)
		gotDownText = true
		
	if gotDown and wantUp and !wantUpText:
		Global.outOfTutorial = true #pengecualian, karena ada poles di sini
		dialog.loadDialog(dialogList)
		print("outOfTutorial is temporarily off") #testing
		wantUpText = true
	
	if wantUpText and !gotUp and !gotUpText and Input.is_action_just_pressed("respawn_start"):
		dialog.loadDialog(dialogList)
		gotUpText = true
	
	if reachCheck and !reachCheckText:
		dialog.loadDialog(dialogList)
		reachCheckText = true
		
	if gotDownCheck and Global.colTileState[1] == "Blue" and !gotDownCheckText: #kalo yg blue nyala
		dialog.loadDialog(dialogList)
		gotDownCheckText = true
		
	if gotDownCheckText and !respawnedCheckText and Input.is_action_just_pressed("respawn_checkpoint"): #kalo udh respawn check
		dialog.loadDialog(dialogList)
		respawnedCheckText = true
		
	if almostPass and !almostPassText and respawnedCheckText: #kalo hampir selesai
		dialog.loadDialog(dialogList)
		almostPassText = true
		
#Tutorial 5
func gotDown():
	gotDown = true
	
func wantUp():
	wantUp = true
	
func reachCheck():
	reachCheck = true
	
func gotDownCheck():
	gotDownCheck = true
	
func almostPass():
	almostPass = true
	
func resetOutOfTutorial():
	Global.outOfTutorial = false
	print("outOfTutorial is back off")
