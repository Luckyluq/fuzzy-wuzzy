extends Node2D

onready var dialog = get_node("Objects/Dialogue/Extra_Big_Dialogue")
var wuzzyRemoved = false
var onAltar = false

var dialogList = [
	"Sometimes you want those tiles to be deactivated to go through. To do that, press [color=#ffc826]F[/color] " \
	+ "when you're on a [color=#001cff]WUZZY-FILLED ALTAR[/color] to remove the Wuzzy.",
	"Good Job! When a Wuzzy is removed, Full Blocks of its color will turn into Invisible Blocks of " \
	+ "that same color. To reactivate it, you need to reinsert that Wuzzy into any empty altars."
	]

func _ready():
	dialog.loadDialog(dialogList)
	
func _process(delta):
	if !wuzzyRemoved and Global.colTileState[1] == "Off":
		wuzzyRemoved()
	
	if wuzzyRemoved: #apakah sudah dilepas
		dialog.loadDialog(dialogList)

#Tutorial 3
func wuzzyRemoved():
	wuzzyRemoved = true
	print("wuzzy removed") #testing
	
