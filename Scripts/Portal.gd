tool
extends Area2D

export var next_scene: PackedScene

func _ready():
	self.connect("body_entered", self, "enterPortal")

func enterPortal(body):
	#anim.play() #benerin
	if body.get_name() == "Fuzzy":
		print("entered Portal" ) #testing
		print("currentWuzzies cleared") #testing
		Global.currentWuzzies = ["","",""]
		print("currentWuzzies is " + str(Global.currentWuzzies)) #testing
		Global.level += 1
		Global.tutorialNum += 1
		#Global.dialogLevel += 1
		Global.resetPartial() #reset karena mau ganti level
		checkOutOfTutorial()
		get_tree().change_scene_to(next_scene)
		#insert sfx	
		Global.checkPassed = false #Balikin status checkPassed

func checkOutOfTutorial(): #cek apakah next levl udh keluar dari tutorial
	if get_parent().name.substr(0,10) == "Tutorial_5":
		#print("it is " + next_scene.node_path.get_name(2).substr(0,5)) #testing
		#if next_scene.name.substr(0,6) == "Level":
		Global.deathCount = 0
		print("reset deatchcount") #testing
		Global.outOfTutorial()
		print("out of tutorial") #testing
	
func _get_configuration_warning() -> String:
	return "Next scene property cannot be empty" if not next_scene else "Clear"
