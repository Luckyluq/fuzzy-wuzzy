extends Area2D

func _ready():
	self.connect("body_entered", self, "entered")

func entered(body):
	var parent = get_parent()
	#Tutorial 2
	if body.get_name() == "Fuzzy" and parent.name == "Tutorial_2":
		if !parent.gotWuzzy(): #kalo nyentuh wuzzy
			parent.gotWuzzy()
			print("gotWuzzy") #testing
	#Tutorial 5
	if body.get_name() == "Fuzzy" and parent.name == "Tutorial_5":
		if !parent.gotDown and self.name == "Area_Trigger": #kalo blm turun
			parent.gotDown()
			print("nama " + self.name)
		elif parent.gotDown and self.name == "Area_Trigger2": #kalo udh turun
			parent.wantUp()
		elif self.name == "Area_Trigger3": #kalo sampe checkpoint
			parent.reachCheck()
		elif self.name == "Area_Trigger4": #kalo udah mau aktivasi altar ke-2
			parent.gotDownCheck()
		elif self.name == "Area_Trigger5": #kalo udah mau selesai tutorial
			parent.almostPass()
		elif self.name == "Area_Trigger6": #kalo udah mau selesai tutorial
			parent.resetOutOfTutorial()
