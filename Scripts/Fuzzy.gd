extends KinematicBody2D

export (int) var speed = 90
export (int) var jump_speed = -175
export (int) var GRAVITY = 900

const UP = Vector2(0,-1)

var inventory = load("res://Scripts/Wuzzy_Inventory.gd").new()
onready var velocity = Vector2()
var isPop = false
var animation
var respawnLoc

func get_input():
	velocity.x = 0
	if is_on_floor():
		if Input.is_action_just_pressed('jump') or Input.is_action_just_pressed("jump_alt"):
			velocity.y = jump_speed
			animation = "jump"
		elif Input.is_action_pressed('right') and isPop == false:
			velocity.x += speed
			animation = "walk"
			$Movement.set_flip_h(false)
			#if not $Footsteps.is_playing():
				#$Footsteps.play()
		elif Input.is_action_pressed('left') and isPop == false:
			velocity.x -= speed
			animation = "walk"
			$Movement.set_flip_h(true)
			#if not $Footsteps.is_playing():
				#$Footsteps.play()
		else:
			if not isPop:
				animation = "idle"
		#elif Input.is_action_just_released("right") or Input.is_action_just_released("left"):
			#$Footsteps.stop()
		
	elif not is_on_floor():
		animation = ""
		#$Footsteps.stop()
		if Input.is_action_pressed('right') and isPop == false:
			$Movement.set_flip_h(false)
			velocity.x += speed
		elif Input.is_action_pressed('left') and isPop == false:
			$Movement.set_flip_h(true)
			velocity.x -= speed
		
	if Input.is_action_just_pressed("respawn_start") and Global.outOfTutorial:
		if !Global.ressingCheck: #avoid bug dimana Fuzzy jadi ilang kalo dipencet Z & C
			velocity.x = 0 #biar pas pop kesannya diam
			animation = "pop"
			#insert SFX
			isPop = true
			Global.ressingStart = true
			print("ressing start is true") #testing
			respawnLoc = "start" #lanjut ke func _on_Movement_animation_finished()
			
		#Hanya bisa respawn di checkpoint kalau sudah dilewati
	if Input.is_action_just_pressed("respawn_checkpoint") and Global.checkPassed and Global.outOfTutorial: 
		if !Global.ressingStart: #avoid bug dimana Fuzzy jadi ilang kalo dipencet Z & C
			velocity.x = 0 #biar pas pop kesannya diam
			animation = "pop"
			#insert SFX
			isPop = true
			Global.ressingCheck = true
			print("ressing check is true") #testing
			respawnLoc = "check" #lanjut ke func _on_Movement_animation_finished()
		
	if Input.is_action_just_pressed("use_wuzzy"):
		inventory.useWuzzy()
		
	if Input.is_action_just_pressed("remove_wuzzy"):
		inventory.removeWuzzy()
		
	if Input.is_action_just_pressed("swap_wuzzy_l"):
		inventory.swap("left")
		
	if Input.is_action_just_pressed("swap_wuzzy_r"):
		inventory.swap("right")
	
	if $Movement.animation != animation:
		$Movement.play(animation)

	if Input.is_action_just_pressed("discard_wuzzy"):
		inventory.discardWuzzy()
		
	if Input.is_action_just_pressed("to_main_menu"):
		get_tree().change_scene("res://Scenes/Main_Menu.tscn")
		Global.resetFull()
		AudioPlayer.stop()
		
	if Input.is_action_just_pressed("reset_level"):
		get_tree().reload_current_scene()
		Global.resetPartial()
		Global.deathCount += 1
		print("after reset " + str(Global.currentWuzzies)) #testing

func _physics_process(delta):
	if not isPop:
		velocity.y += delta * GRAVITY
	else:
		velocity.y = 0 #biar pas pop kesannya diam
	get_input()
	velocity = move_and_slide(velocity, UP)
	
func _on_Movement_animation_finished():
	if $Movement.animation == 'pop':
		Global.deathCount += 1
		isPop = false
		if Global.ressingStart:
			Global.ressingStart = false
			print("ressing start is back to false") #testing
		elif Global.ressingCheck:
			Global.ressingCheck = false
			print("ressing check is back to false") #testing
	
	#Biar kalo udah selesai animasi Pop, baru respawn
	if respawnLoc == "start":
		self.position = get_parent().get_node("Poles").get_node("Pole_Start").position
		respawnLoc = ""
		print("respawned at Start") #testing
		
	if respawnLoc == "check": 
		self.position = get_parent().get_node("Poles").get_node("Pole_Check").position
		respawnLoc = ""
		print("respawned at Check") #testing
